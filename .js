/* Short answer: don't. Use conditional branches instead. See below. */

Promise.try(function(){
	return someAsyncThing();
}).then(function(value){
	if (value === 3) {
		return "final value";
	} else {
		return Promise.try(function(){
			return someOtherAsyncThing();
		}).then(function(secondValue){
			return getFinalValue(secondValue);
		});
	}
})